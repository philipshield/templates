﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Linq.Expressions;
using System.Reflection;

namespace GenerateMakeSubmitSolution
{
    public static class E
    {
        #region .With


        /// <summary>
        /// Provides a neat way of using String-Formats
        /// </summary>
        /// <param name="source">The string source.</param>
        /// <param name="objects">The format objects.</param>
        /// <example>"My sample string is {0} and {1}".With("Awesome", "convenient");</example>
        /// <returns>the format</returns>
        public static string With(this string source, params object[] objects)
        {
            if (source != null)
            {
                return String.Format(source, objects);
            }
            throw new NullReferenceException("source string is null");
        }

        /// <summary>
        /// Provides formatting functionallity with custom keywords.
        /// Creator: Bartdesmets.
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <param name="args">The formatting.</param>
        /// <example>"myString {city} , {object}".Format(city => "New York", object => new Object()</example>
        /// <returns>A formatted copy of the string.</returns>
        public static string With(this string source, params Expression<Func<string, object>>[] expressions)
        {
            // {parameters of expressions}  ----> is to be replaced by corresponding ----> "return value of expression"
            Dictionary<string, object> formatpairs = expressions.ToDictionary
                                                     (e => "{" + e.Parameters[0].Name + "}"
                                                    , e => e.Compile()(null));

            var s = new StringBuilder(source);
            foreach (var kv in formatpairs)
            {
                s.Replace(

                    kv.Key,
                    kv.Value != null
                          ? kv.Value.ToString()
                          : "");
            }

            return s.ToString();
        }

        #endregion
    }
}
