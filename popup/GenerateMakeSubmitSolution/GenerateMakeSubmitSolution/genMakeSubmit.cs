﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GenerateMakeSubmitSolution
{
    class genMakeSubmit
    {
        static void Main(string[] args)
        {
            //CREATE SUBMIT FILE
            Console.WriteLine("writing submit.py");
            string pysourcefile = @"C:\Users\Philip\Dropbox\Code\Config\submit.py";
            string pytargetfile = Path.Combine(Directory.GetCurrentDirectory(), @"submit.py");
            if (!File.Exists(pytargetfile))
                File.Copy(pysourcefile, pytargetfile);
            Console.WriteLine("success!");
          

            //CREATE MAKEFILE
            Console.WriteLine();
            Console.WriteLine("Creating MAKEFILE:");
            Console.Write("kattis id:");
            string kattisid = Console.ReadLine();

            string makefilecontents = 
(@"
SRCS = $(wildcard *.cpp)
SRCS_ALL = $(wildcard *.cpp)
HDRS = $(wildcard *.h)

OUTPUTFILE = {s_kattisid}.exe
KATTISID = {s_kattisid}
KATTISFLAGS = -f
KATTIS = ./submit.py $(KATTISFLAGS) -p $(KATTISID) $(SRCS) $(HDRS)
#KATTIS = echo $(SRCS) $(HDRS)

submit:
	$(KATTIS)

").With(s_kattisid => kattisid);


            string maketargetfile = "makefile";
            if (!File.Exists(maketargetfile))
            {
                using (System.IO.StreamWriter makefile = new System.IO.StreamWriter(maketargetfile))
                {
                    makefile.WriteLine(makefilecontents);
                }

                Console.WriteLine("success. MAKEFILE created");

            }
            else 
            {
                Console.WriteLine("MAKEFILE exists.");
            }

            



            Console.WriteLine("exiting.");

        }
    }
}
