#ifndef _COMMON_H
#define _COMMON_H


//
//+ Includes
//

#include <cstdlib>
#include <utility>
#include <algorithm>
#include <cmath>
#include <complex>
#include <limits>
#include <climits>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <queue>
#include <stack>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <cassert>
#include <functional>
#include <chrono>
#include <memory>


#ifdef LOCAL
#include "Debug.h"
#include <Windows.h>
#include <cstdlib>
#endif


using namespace std;
using namespace std::chrono;

#define rep(i, a, b) for (int i = (a); i < (b); ++i)
#define rrep(i, b, a) for (int i = (b-1); i >= a; --i)
#define urep(i, a, b) for (size_t i = (a); i < (b); ++i)
#define trav(it, v) for (typeof((v).begin()) it = (v).begin(); it != (v).end(); ++it)
#define log_2(x) (log((double)x)/log(2.0))
#define logf_2(x) (logf(float(x))/logf(2.0f))
#define randf() (float(rand())/RAND_MAX)
#define sgn(x) ((x > 0) ? 1 : ((x < 0) ? -1 : 0))


#ifdef WIN32
// Windows

#ifndef LOCAL
#error Build with LOCAL
#endif

#define typeof decltype
#define isnan(x) (_isnan(x))
#define __builtin_expect(exp, c) (exp)
#define round(x) floor((x) + 0.5)

#else
// KATTIS
#endif


//
//+ typedefs
//


typedef long long ll;
typedef unsigned long long ull;
typedef vector<int> vi;
typedef vector<unsigned> vu;
typedef vector<float> vf;
typedef vector<double> vd;
typedef pair<int, int> pii;

typedef complex<float> pointf;
typedef complex<int> pointi;
#define X(p) ((p).real())
#define Y(p) ((p).imag())
#define endline "\n"


//
//+ Globals 
//

#ifdef LOCAL
// Time point at which the program was started.
// This does not account for some statically initialized variables.
extern high_resolution_clock::time_point gStart;
#endif

//
//+ Helper Functions and Classes
//

#ifdef LOCAL
enum DefaultLocalRun
{
	Stdin,
	File1in
};
#endif


//
//+ KATTIS OUTPUT
//

#ifdef LOCAL
#define output(x) \
{ \
	Display::setTextColor(CColor::LIGHTCYN); \
	cout << x; \
	Display::setTextColor(CColor::WHITE); \
}
#else
#define output(x) \
{ \
	cout << x; \
}

//redefine kattin as cin on KATTIS
#define kattin cin

#endif




// Get the i'th field of a pair, were i is either 0 or 1.
template<class T>
T& pair_part(pair<T, T>& p, int i)
{
	return (i == 0) ? p.first : p.second;
}

#define Now() (high_resolution_clock::now())

namespace std
{
	ostream& operator<<(ostream& out, const vi& v);
	ostream& operator<<(ostream& out, const vu& v);
	ostream& operator<<(ostream& out, const vector<string>& v);
};

class misc_error : public exception { };
class not_implemented_exception : public exception { };


#endif
