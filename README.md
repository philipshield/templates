# README #

This repository is a collection of project templates that I find useful! 

Feel free to use any of them!

## Whats Included? ##
### Popup ###
Problem Solving and Programming under Pressure - Templates for rapid algorithm construction, including utilities for working with online judge KATTIS.

* **generate make-submit** - generates a makefile for kattis submission
* **popup c++ template** - c++ algorithm template
* **popup c# template** - c# algorithm template

### Unity ###
* Coming up