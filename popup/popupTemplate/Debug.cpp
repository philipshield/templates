#include "Common.h"
#include "Debug.h"


#ifdef LOCAL

PrintType DEBUG_LEVEL = PrintType::Info;
static size_t sIndent;
const size_t INDENT_SIZE = 3;
static high_resolution_clock::time_point sLastTime;

PrintLabel::PrintLabel(PrintType type) :
_type(type)
{
}

static string PrintTypeName(PrintType type)
{
	switch (type)
	{
	case Trace:
		Display::setTextColor(CColor::DARKGRA);
		return "trace";
	case Info:
		Display::setTextColor(CColor::GREEN);
		return "info";
	case Warning:
		Display::setTextColor(CColor::YELLOW);
		return "WARNING";
	case Error:
		Display::setTextColor(CColor::LIGHTRE);
		return "ERROR";
	default:
		throw not_implemented_exception();
	}
}

namespace std
{
	ostream& operator<<(ostream& out, const PrintLabel& printLabel)
	{
		auto now = high_resolution_clock::now();
		auto diff = now - gStart;
		auto diffLast = (sLastTime.time_since_epoch().count() > 0) ?
			now - sLastTime : high_resolution_clock::duration();
		out	<< endl << "[" << setw(6) << setprecision(3) << fixed << duration_cast<microseconds>(diff).count() / 1e6 << "]"
					<< "[+" << setw(5) << setprecision(3) << fixed << duration_cast<microseconds>(diffLast).count() / 1e6 << "]"
					<< "[" << setw(7) << PrintTypeName(printLabel._type) << "]"
					<< " " << string(sIndent * INDENT_SIZE, ' ');
		sLastTime = now;
		Display::setTextColor(CColor::WHITE);
		return out;
	}
};

void Display::setTextColor(CColor color) {
	HANDLE cHandle;
	cHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(cHandle, color);
}

#endif

void PrintIndent()
{
#ifdef LOCAL
	++sIndent;
#endif
}

void PrintUnindent()
{
#ifdef LOCAL
	if (sIndent == 0)
		throw misc_error();
	--sIndent;
#endif
}

