#include "Common.h"

template <class T>
static ostream& PrintVector(ostream& out, const vector<T>& v)
{
	if (v.empty())
		return out;
	out << v[0];
	urep(i, 1, v.size())
		out << " " << v[i];
	return out;
}

namespace std
{
	ostream& operator<<(ostream& out, const vi& v)
	{
		return PrintVector(out, v);
	}

	ostream& operator<<(ostream& out, const vu& v)
	{
		return PrintVector(out, v);
	}

	ostream& operator<<(ostream& out, const vector<string>& v)
	{
		return PrintVector(out, v);
	}
};