#include "Common.h"
#include "Debug.h"

// REMEMBER:
//+ endl in end of solution!!!
//+ use kattin instead of cin
//+ use output() instead of cout
//+ breakpoints at "<-- SET BREAKPOINT"
//+ standard local file is: Input\\1.in
//+ set gUseDeadline to true if using deadlines

//
//+ Deadline & Time
//

#ifdef LOCAL
high_resolution_clock::time_point gStart(high_resolution_clock::now());
#endif

high_resolution_clock::time_point gDeadline(high_resolution_clock::now()
	+ seconds(2) - milliseconds(90));

bool gUseDeadline(false);

bool deadline()
{
/*
#ifdef _DEBUG
	return false;
#endif
*/
	bool time_is_up = gUseDeadline && Now() > gDeadline;
	if (time_is_up)
		Debug(Warning, "Time is up!");
	return time_is_up;
}

#ifdef LOCAL
ifstream kattin;
static DefaultLocalRun defaultrun = DefaultLocalRun::File1in;
#endif


//
//+ Solution
//

int _t = 1;
void solve()
{
	Debug(Trace, "Entering Solve");
	Debug(Error, "TestError");
	Debug(Warning, "TestWarning");

	// SOLVE:
	


	Debug(Trace, "Leaving Solve"); // <-- SET BREAKPOINT
}





//
//+ Generate Test Case
//

#ifdef LOCAL
ofstream testcase;
bool generateTestCase()
{
	return true;
}
#endif



//
//+ Main Entry Point
//

static vector<string> sArgs;
static function<bool(string)> sHasArg;
int main(int argc, char* argv[])
{
	Debug(Trace, "Entering main");
	ios::sync_with_stdio(false); // do not sync cin and cout
	cin.tie(NULL); // do not flush cout when using cin (for alternate cout and cin..)

#ifdef LOCAL
	srand((unsigned int)time(NULL));
	DEBUG_LEVEL = PrintType::Trace;
	sArgs.assign(&(argv[1]), &(argv[argc]));
	Debug(Info, "Args: " << sArgs);
	sHasArg = [](string a) { return find(sArgs.begin(), sArgs.end(), a) != sArgs.end(); };

	if (sHasArg("-i") || sHasArg("--interactive"))
	{
		Debug(Info, "Interactive Mode");
		int i = 1;
		cout << "What do we do?" << endline;
		cout << i++ << " > We drop it" << endline;
		cout << i++ << " > Run a case from stdin" << endline;
		cout << i++ << " > Run a case from file" << endline;
		cout << i++ << " > Generate a test case" << endline;
		cout << endline << "> ";
		int pick;
		while (!(cin >> pick) || pick < 1 || pick >= i);
		string filename, path, msg;
		switch (pick)
		{
		case 1: // Drop it
			exit(0);
		case 2:	 // stdin
			Debug(Trace, "reading from stdin");
			kattin = ifstream(stdin);
			break;
		case 3: //file
			filename;
			cout << "filename: Input\\";
			cin >> filename;
			path = "Input\\" + filename;
			msg = "Running test case: " + path;
			Debug(Info, msg);
			kattin = ifstream(path);

			if (!kattin.is_open())
				Debug(Error, "specified file cannot be opened");
			break;

		case 4: //Genarate test case
			string tfilename;
			cout << "filename: Input\\";
			cin >> tfilename;
			string tpath = "Input\\" + tfilename;
			testcase = ofstream(tpath);
			Debug(Info, "Generating test case..");
			if (generateTestCase())
			{
				string tmsg = "Created test case: " + tpath;
				Debug(Info, tmsg);
			}
			else
			{
				Debug(Warning, "something went wrong. test case not generated.");
			}

			Debug(Trace, "exiting...");
			int wait; cin >> wait;
			exit(0);


		}

	}
	else
	{
		//not interactive
		switch (defaultrun)
		{
		case DefaultLocalRun::Stdin:
			Debug(Info, "Reading from stdin");
			kattin = ifstream(stdin);
			break;
		case DefaultLocalRun::File1in:
			Debug(Info, "Reading from Input\\1.in");
			kattin = ifstream("Input\\1.in");
			break;
		default:
			Debug(Info, "Reading from stdin");
			kattin = ifstream(stdin);
			break;
		}
		
			
		



	}
#else // KATTIS
	//kattin is redefined as cin on KATTIS
#endif


	//
	//+ INPUT & SOLVE
	//

	solve();


	Debug(Trace, "Leaving Main");
	return 0;
}




