﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace popupTemplateCSharp
{
    static class Kattio
    {
        static readonly char[] delimiter = " ".ToCharArray();
        static string[] buffer = null;
        static int bufferIndex = 0;

        public static T Read<T>()
        {
            if (buffer == null)
            {
                buffer = Console.ReadLine().Split(delimiter);
                bufferIndex = 0;
            }
            T result = (T)Convert.ChangeType(buffer[bufferIndex], typeof(T));
            if (++bufferIndex == buffer.Length)
                buffer = null;
            return result;
        }
    }
}